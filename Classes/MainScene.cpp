#include "MainScene.h"
#include "SimpleAudioEngine.h"

USING_NS_CC;

Scene* MainScene::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = MainScene::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool MainScene::init()
{
    if (!CCLayerColor::initWithColor(ccc4(255, 245, 195, 255)) )
    {
        return false;
    }
    
    visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();

    auto listener = EventListenerTouchOneByOne::create();
    listener->setSwallowTouches(true);
    listener->onTouchBegan = CC_CALLBACK_2(MainScene::onTouchBegan, this);
    listener->onTouchMoved = CC_CALLBACK_2(MainScene::onTouchMoved, this);
    listener->onTouchEnded = CC_CALLBACK_2(MainScene::onTouchEnded, this);
    listener->onTouchCancelled = CC_CALLBACK_2(MainScene::onTouchCancelled, this);
    auto dispatcher = Director::getInstance()->getEventDispatcher();
    dispatcher->addEventListenerWithSceneGraphPriority(listener, this);
    
    scheduleUpdate();
    
    isTouchDown = false;
    
    initialTouchPos[0] = 0;
    initialTouchPos[1] = 0;
    
    auto closeItem = MenuItemImage::create(
                                           "CloseNormal.png",
                                           "CloseSelected.png",
                                           CC_CALLBACK_1(MainScene::menuCloseCallback, this));
    
    closeItem->setPosition(Vec2(origin.x + visibleSize.width - closeItem->getContentSize().width/2 ,
                                origin.y + closeItem->getContentSize().height/2));

    auto menu = Menu::create(closeItem, NULL);
    menu->setPosition(Vec2::ZERO);
    this->addChild(menu, 1);
    
    titleLabel = Label::createWithTTF("COMBI", "fonts/arial.ttf", 22);
    titleLabel->setTextColor(Color4B(0, 0, 0, 255));
    
    // position the label on the center of the screen
    titleLabel->setPosition(Vec2(origin.x + visibleSize.width/2,
                            origin.y + visibleSize.height - titleLabel->getContentSize().height));

    this->addChild(titleLabel, 1);
    
    // Place the grid
    for(int i = 0; i < GRID_WIDTH; i++)
    {
        for(int j = 0; j < GRID_HEIGHT; j++)
        {
            auto sprite = Sprite::create("blue_square.png");
            sprite->setPosition(Vec2(visibleSize.width / 2 + origin.x + (i - GRID_WIDTH / 2) * 32, visibleSize.height/2 + origin.y + (j - GRID_HEIGHT / 2) * 32));
            sprite->setScale(1, 1);
            grid[i][j] = sprite;
            this->addChild(sprite, 0);
        }
    }
    
    // Create the pieces)
    CirclePiece* piece = new CirclePiece(Vec2(1, 2), Vec2(visibleSize.width / 2 + origin.x - GRID_WIDTH / 2 * 32, visibleSize.height / 2 + origin.y - GRID_HEIGHT / 2 * 32), Vec2(GRID_WIDTH, GRID_HEIGHT), Red);
    CirclePiece* piece2 = new CirclePiece(Vec2(2, 2), Vec2(visibleSize.width / 2 + origin.x - GRID_WIDTH / 2 * 32, visibleSize.height/2 + origin.y - GRID_HEIGHT / 2 * 32), Vec2(GRID_WIDTH, GRID_HEIGHT), Blue);
    CirclePiece* piece3 = new CirclePiece(Vec2(0, 1), Vec2(visibleSize.width / 2 + origin.x - GRID_WIDTH / 2 * 32, visibleSize.height/2 + origin.y - GRID_HEIGHT / 2 * 32), Vec2(GRID_WIDTH, GRID_HEIGHT), Purple);
    addChild(piece->getSprite(), 0);
    addChild(piece2->getSprite(), 0);
    addChild(piece3->getSprite(), 0);
    
    pieces[0] = piece;
    pieces[1] = piece2;
    pieces[2] = piece3;
    
    return true;
}

bool MainScene::onTouchBegan(Touch* touch, Event* unused_event)
{
    initialTouchPos[0] = touch->getLocation().x;
    initialTouchPos[1] = touch->getLocation().y;
    currentTouchPos[0] = touch->getLocation().x;
    currentTouchPos[1] = touch->getLocation().y;
    
    isTouchDown = true;
    
    return true;
}

void MainScene::onTouchMoved(Touch *touch, Event *event)
{
    currentTouchPos[0] = touch->getLocation().x;
    currentTouchPos[1] = touch->getLocation().y;
}

void MainScene::onTouchEnded(Touch *touch, Event *event)
{
    isTouchDown = false;
}

void MainScene::onTouchCancelled(Touch *touch, Event *event)
{
    onTouchEnded(touch, event);
}

void MainScene::update(float dt)
{
    if (!isMoving && isTouchDown)
    {
        Direction direction = None;
        if (initialTouchPos[0] - currentTouchPos[0] > visibleSize.width * 0.05) // Left
        {
            isTouchDown = false;
            direction = Left;
        }
        else if (initialTouchPos[0] - currentTouchPos[0] < - visibleSize.width * 0.05) // Right
        {
            isTouchDown = false;
            direction = Right;
        }
        else if (initialTouchPos[1] - currentTouchPos[1] > visibleSize.width * 0.05) // Down
        {
            isTouchDown = false;
            direction = Down;
        }
        else if (initialTouchPos[1] - currentTouchPos[1] < - visibleSize.width * 0.05) // Up
        {
            isTouchDown = false;
            direction = Up;
        }
        
        if(direction != None)
        {
            isMoving = true;
            for(int i = 0; i < 5; i++)
            {
                if(pieces[i])
                {
                    pieces[i]->goInDirection(direction);
                }
            }
        }
    }
    
    if(isMoving)
    {
        bool isReadyForCheck = true;
        
        for(int i = 0; i < 5; i++)
        {
            if(pieces[i])
            {
                if(pieces[i]->getMovementMode() != MovementMode::HasMoved)
                {
                    isReadyForCheck = false;
                    break;
                }
            }
        }
        
        if(isReadyForCheck)
        {
            checkForColorChanges();
            isMoving = false;
        }
    }
}

void MainScene::checkForColorChanges()
{
    for(int i = 0; i < 5; i++)
    {
        if(pieces[i])
        {
            for(int j = 0; j < 5; j++)
            {
                if(pieces[j])
                {
                    if(pieces[i]->getId() != pieces[j]->getId())
                    {
                        if((int)pieces[i]->getPosition().x == (int)pieces[j]->getPosition().x && (int)pieces[i]->getPosition().y == (int)pieces[j]->getPosition().y)
                        {
                            if(pieces[i]->getColor() == pieces[j]->getColor())
                            {
                                // Pop both
                                removeChild(pieces[i]->getSprite());
                                removeChild(pieces[j]->getSprite());
                                pieces[i] = 0;
                                pieces[j] = 0;
                            }
                            else // Find combined color!
                            {
                                for(auto colorCombination : combinations)
                                {
                                    if((colorCombination.first == pieces[i]->getColor() && colorCombination.second == pieces[j]->getColor()) ||
                                       (colorCombination.first == pieces[j]->getColor() && colorCombination.second == pieces[i]->getColor()))
                                    {
                                        // Pop both and create new color
                                        removeChild(pieces[i]->getSprite());
                                        removeChild(pieces[j]->getSprite());
                                        
                                        Vec2 origin = Director::getInstance()->getVisibleOrigin();
                                        
                                        pieces[i] = new CirclePiece(pieces[i]->getPosition(), Vec2(visibleSize.width / 2 + origin.x - GRID_WIDTH / 2 * 32, visibleSize.height / 2 + origin.y - GRID_HEIGHT / 2 * 32), Vec2(GRID_WIDTH, GRID_HEIGHT), colorCombination.combined);
                                        addChild(pieces[i]->getSprite());
                                        pieces[j] = 0;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    for(int i = 0; i < 5; i++)
    {
        if(pieces[i])
        {
            pieces[i]->setMovementMode(MovementMode::NotMoving);
        }
    }
}

void MainScene::menuCloseCallback(Ref* pSender)
{
    //Close the cocos2d-x game scene and quit the application
    Director::getInstance()->end();

    #if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    exit(0);
#endif
    
    /*To navigate back to native iOS screen(if present) without quitting the application  ,do not use Director::getInstance()->end() and exit(0) as given above,instead trigger a custom event created in RootViewController.mm as below*/
    
    //EventCustom customEndEvent("game_scene_close_event");
    //_eventDispatcher->dispatchEvent(&customEndEvent);
}
