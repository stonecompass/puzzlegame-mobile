#ifndef __HELLOWORLD_SCENE_H__
#define __HELLOWORLD_SCENE_H__

#include "cocos2d.h"

#include "CirclePiece.h"

using namespace cocos2d;

struct ColorCombination
{
    Color first;
    Color second;
    
    Color combined;
};

class MainScene : public cocos2d::CCLayerColor
{
public:
    static cocos2d::Scene* createScene();

    virtual bool init();
    
    // a selector callback
    void menuCloseCallback(cocos2d::Ref* pSender);
    // implement the "static create()" method manually
    CREATE_FUNC(MainScene);
    
    bool onTouchBegan(Touch *touch, Event *unused_event);
    void onTouchMoved(cocos2d::Touch *touch, cocos2d::Event *event);
    void onTouchEnded(cocos2d::Touch *touch, cocos2d::Event *event);
    void onTouchCancelled(cocos2d::Touch *touch, cocos2d::Event *event);
    void update(float dt);
private:
#define GRID_WIDTH 3
#define GRID_HEIGHT 2
    void checkForColorChanges();
    bool isMoving;
    bool isTouchDown;
    float initialTouchPos[2];
    float currentTouchPos[2];
    cocos2d::Size visibleSize;
    Sprite* player;
    Vec2 currentPosition;
    Sprite* grid[GRID_WIDTH][GRID_HEIGHT];
    Label* titleLabel;
    CirclePiece* pieces[5];
    
    std::vector<ColorCombination> combinations = { { Color::Red, Color::Blue, Color::Purple }};
};

#endif // __HELLOWORLD_SCENE_H__
